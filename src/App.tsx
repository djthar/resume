import React from "react";
import "bootstrap";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/js/bootstrap.js";
import './App.css';
import NavBar from "./components/NavBar"
import About from "./components/About"
import Hero from "./components/Hero";
import Resume from "./components/Resume";
import Contact from "./components/Contact";
import AOS from 'aos';
import 'aos/dist/aos.css';
import Technologies from "./components/Technologies";
import Languages from "./components/Languages";


function App() {
    AOS.init()

    return (
        <>
            <NavBar/>
            <Hero/>
            <About/>
            <Languages/>
            <Technologies/>
            <Resume/>
            <Contact/>
        </>
    );
}

export default App;
