import React from "react";
import {BiChevronRight} from "react-icons/all";

const About = () => {
    return (
        <section id="about" className="about">
            <div className="container">

                <div className="section-title">
                    <h2>About</h2>
                    <p>I'm a positive and happy person that isn't afraid of challenges. I approach the challenges with a
                        smile and an open mind.<br/>
                        I like technology so I like to be up to date with new technologies and test them by myself. I'm
                        always willing to learn new things and to expand the knowledge among my mates.<br/>
                        My other half is bound to sports, specifically swimming. I've practice it since I was 7 and I'm
                        still doing it. I like to take part in competitions and share the sport and trainings with my
                        friends.</p>
                </div>

                <div className="row">
                    <div className="col-lg-4" data-aos="fade-right">
                        <img src={process.env.PUBLIC_URL + '/img/profile-img.png'} alt="" className="img-fluid" style={{background: "white"}}/>
                    </div>
                    <div className="col-lg-8 pt-4 pt-lg-0 content" data-aos="fade-left">
                        <h3>Software Engineer &amp; Software Architect.</h3>
                        <p className="fst-italic">
                            I'm a experienced Software Engineer and Software Architect. I like designing complex systems
                            while maintaining low coupling and a good test coverage.<br/>
                            I write performant code by writing clean code.
                        </p>
                        <div className="row">
                            <div className="col-lg-6">
                                <ul>
                                    <li><BiChevronRight/> <strong>Birthday:</strong> <span>7 Oct 1982</span></li>
                                    <li><BiChevronRight/> <strong>Phone:</strong> <span>+34 615 362 975</span></li>
                                    <li><BiChevronRight/> <strong>City:</strong> <span>Madrid, Spain</span></li>
                                </ul>
                            </div>
                            <div className="col-lg-6">
                                <ul>
                                    <li><BiChevronRight/> <strong>Age:</strong><span>38</span></li>
                                    <li><BiChevronRight/> <strong>Degree:</strong><span>Master</span></li>
                                    <li><BiChevronRight/> <strong>Email:</strong><span>miguel.a.j82@gmail.com</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    );
};

export default About;
