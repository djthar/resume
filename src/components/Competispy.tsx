import {BsPrefixProps, Omit} from "react-bootstrap/helpers";
import React, {useState} from "react";
import {Button, Modal, ModalProps} from "react-bootstrap";

const CompetispyModal = (props: JSX.IntrinsicAttributes & Omit<Pick<React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>, "key" | keyof React.HTMLAttributes<HTMLDivElement>> & { ref?: ((instance: HTMLDivElement | null) => void) | React.RefObject<HTMLDivElement> | null | undefined; }, BsPrefixProps<"div"> & ModalProps> & BsPrefixProps<"div"> & ModalProps & { children?: React.ReactNode; }) => {
    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Competispy experience
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p>The <a href="https://www.competispy.com">Competispy</a> web application offers a clean interface to
                    make it possible for the different participants in selected spanish swimming meetings to search
                    their position in the inscriptions, filtered by category, and to look at the different participants'
                    best records to make a proper comparison.<br/>
                    It uses a backend written in <strong>Python</strong> using <strong>Flask</strong> that exposes
                    a <strong>REST API</strong>.<br/>
                    A frontend written in <strong>Typescript</strong> and <strong>React</strong> that consumes the
                    backend API. The backend uses a <strong>MongoDB</strong> database as a source of data.<br/>
                    The backend and frontend are automatically built as <strong>Docker</strong> images using <strong>Gitlab
                        CI</strong>.<br/>
                    The project is deployed in a <strong>cloud</strong> hosted by Hetzner where I run a
                    small <strong>Kubernetes</strong> cluster based on K3D.
                    <br/>
                    The infrastructure is automatically generated
                    with <strong>Terraform</strong> and <strong>ArgoCD</strong> is used to track and install
                    the applications latest versions.<br/>
                    I leverage the use of the controllers such as cert-manager to do automatic renewal of HTTPS
                    certificates.<br/>
                    The backend API is exposed for other services such as Alexa Skins to consume it.<br/>
                    Some extensions for the API are done as different microservice written
                    in <strong>Go</strong> using <strong>JWT</strong> for
                    authentication.<br/>
                    I’ve been using this project over the years to put into practice some of the knowledge gained in the
                    courses I’ve attended or to learn new technologies.
                </p>
                <ul>
                    <li>Developed a web application from scratch</li>
                    <li>Maintained over the years</li>
                    <li>Dockerized the solution</li>
                    <li>Moved it from <strong>Red Hat Open Shift</strong> to <strong>Google App Engine</strong></li>
                    <li>Moved from Google App Engine to a Kubernetes cluster</li>
                    <li>Deployed in a cloud provider with Terraform</li>
                    <li>Automatized the CI with Argo</li>
                </ul>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={props.onHide}>Close</Button>
            </Modal.Footer>
        </Modal>
    );
};

const Competispy = () => {
    const [modalShow, setModalShow] = useState(false);
    return (
        <div className="resume-item">
            <h4>Full Stack Software Engineer</h4>
            <h5>2015 - Present</h5>
            <p><em><a href="https://www.competispy.com">Competispy</a>, Madrid, Madrid </em></p>
            <Button variant="primary" onClick={() => setModalShow(true)}>
                View
            </Button>
            <CompetispyModal
                show={modalShow}
                onHide={() => setModalShow(false)}
            />
        </div>
    );
};

export default Competispy;
