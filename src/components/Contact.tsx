import React from "react";
import ContactUs from "./ContactUs";

const Contact = () => {
    return (
        <section id="contact" className="contact">
            <div className="container">
                <div className="section-title">
                    <h2>Contact</h2>
                    <p>If you want to contact me you can simply drop me an e-mail or use the following form and I'll
                        answer as soon as possible.</p>
                </div>
                <ContactUs/>
            </div>
        </section>
    );
};

export default Contact;
