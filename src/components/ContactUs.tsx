import React, {useState} from 'react';
import emailjs from 'emailjs-com';
import {Modal, Button} from "react-bootstrap";

export default function ContactUs() {
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);

    // @ts-ignore
    function sendEmail(e) {
        e.preventDefault();

        emailjs.sendForm('service_lg0rcgz', 'template_6emjvym', e.target, 'user_WFE0K4c9vmXAIWhK5yB5l')
            .then((result) => {
                console.log(result.text);
            }, (error) => {
                console.log(error.text);
            });
        setShow(true);
    }

    return (
        <>
            <form onSubmit={sendEmail} className="email-form">
                <div className="row">
                    <div className="form-group col-md-6">
                        <label>Your Name</label>
                        <input type="text" name="user_name" className="form-control" id="user_name" required/>
                    </div>
                    <div className="form-group col-md-6">
                        <label>Your Email</label>
                        <input type="email" className="form-control" name="user_email" id="user_email" required/>
                    </div>
                </div>
                <div className="form-group">
                    <label>Subject</label>
                    <input type="text" className="form-control" name="user_subject" id="subject" required/>
                </div>
                <div className="form-group">
                    <label>Message</label>
                    <textarea className="form-control" name="message" rows={10} required/>
                </div>
                <div className="my-3">
                    <div className="loading">Loading</div>
                    <div className="error-message"/>
                    <div className="sent-message">Your message has been sent. Thank you!</div>
                </div>
                <div className="text-center">
                    <button type="submit">Send Message</button>
                </div>
            </form>
            <Modal show={show} onHide={handleClose}>
                <Modal.Body>Thank you for your message. I'll answer you soon.</Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Close
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}
