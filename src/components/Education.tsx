import SoftwareCraftsmanship from "./SoftwareCraftsmanship";
import React from "react";

const Education = () => {
    return (
        <div className="col-lg-6" data-aos="fade-up">
            <h3 className="resume-title">Education</h3>
            <SoftwareCraftsmanship/>
            <div className="resume-item">
                <h4>Managing Cloud Infrastructure with Terraform</h4>
                <h5>2021 - 2021</h5>
                <p><em>Coursera, Online</em></p>
            </div>
            <div className="resume-item">
                <h4>KBS-105: Docker Intro + Kubernetes & Helm Admin with CKA & CKAD</h4>
                <h5>2020 - 2020</h5>
                <p><em>ComponentSoft, Madrid, Madrid</em></p>
                <p>Preparation for Kubernetes administration exam covering Docker, Kubernetes and Helm</p>
            </div>
            <div className="resume-item">
                <h4>Getting Started with Google Kubernetes Engine</h4>
                <h5>2019 - 2019</h5>
                <p><em>Coursera, Online</em></p>
            </div>
            <div className="resume-item">
                <h4>Game Theory</h4>
                <h5>2013 - 2013</h5>
                <p><em>Coursera, Online</em></p>
            </div>
            <div className="resume-item">
                <h4>AI for Robotics: Programming a Robotic Car</h4>
                <h5>2012 - 2012</h5>
                <p><em>Udacity, Online</em></p>
            </div>
            <div className="resume-item">
                <h4>Introduction to AI</h4>
                <h5>2011 - 2011</h5>
                <p><em>Stanford, Online</em></p>
            </div>
            <div className="resume-item">
                <h4>Machine Learning</h4>
                <h5>2011 - 2011</h5>
                <p><em>Stanford, Online</em></p>
            </div>
            <div className="resume-item">
                <h4>Bachelor of Telecommunication Engineer</h4>
                <h5>2000 - 2009</h5>
                <p><em>Universidad Alcalá de Henares, Alcalá de Henares, Madrid</em></p>
                <p>I studied the telematics specialization, being more exposed to networking, protocols, routers and web development</p>
            </div>
        </div>
    );
};

export default Education;
