import {BsPrefixProps, Omit} from "react-bootstrap/helpers";
import React, {useState} from "react";
import {Button, Modal, ModalProps} from "react-bootstrap";

const EricssonModal = (props: JSX.IntrinsicAttributes & Omit<Pick<React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>, "key" | keyof React.HTMLAttributes<HTMLDivElement>> & { ref?: ((instance: HTMLDivElement | null) => void) | React.RefObject<HTMLDivElement> | null | undefined; }, BsPrefixProps<"div"> & ModalProps> & BsPrefixProps<"div"> & ModalProps & { children?: React.ReactNode; }) => {
    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Ericsson experience
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p>I’ve been involved in the development of the GGSN node of the UMTS architecture and in the UPF node
                    of the 5G architecture.<br/>
                    I’ve been working in the traffic analysis component, capable of determining the source of traffic
                    coming from a device and route and charge appropriately.<br/>
                    This software, written in <strong>C</strong> and <strong>C++</strong> with a great number of tools
                    in <strong>Python</strong>, is mainly used to give
                    more tailored payment packages to customers depending on what type of services they want to use, for
                    example social media or streaming services.<br/>
                    I've learned and evolved from simply writing code to create good designs, document and share them.
                    I've been able to join the <strong>Architecture</strong> team were I participate in all the phases
                    of the
                    features development.<br/>
                    I've made efforts to increase the quality of the code delivered by the teams, so I've been highly
                    involved in the development of the <strong>CI framework</strong>.
                </p>
                <ul>
                    <li>Study and design solution for several new features of the product. Mainly focused
                        on <strong>Cloud</strong> and
                        stand alone feature parity
                    </li>
                    <li>Improved the automatic build and test pipeline by adding tools for static code analysis and code
                        coverage
                    </li>
                    <li>Improved the quality of the code by increasing the test coverage
                        and <strong>refactoring</strong> the code
                    </li>
                    <li>Improved the compilation system to lower the compilation times</li>
                    <li>Give several <strong>talks</strong> about good coding practices and <strong>smell codes</strong>
                    </li>
                    <li>Added a ML engine in the product with focus on low latency</li>
                    <li>Designed the software to export learned models from <strong>SciKit Learn</strong> to out
                        custom <strong>ML models</strong> representation
                    </li>
                    <li>Improved the accuracy of the classification system by using new ML models</li>
                    <li><strong>Mentored</strong> over 6 newcomers and scholars with great success</li>
                </ul>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={props.onHide}>Close</Button>
            </Modal.Footer>
        </Modal>
    );
};

const Ericsson = () => {
    const [modalShow, setModalShow] = useState(false);
    return (
        <div className="resume-item">
            <h4>Software Engineer and Software Architect</h4>
            <h5>2013 - Present</h5>
            <p><em>Ericsson, Madrid, Madrid </em></p>
            <Button variant="primary" onClick={() => setModalShow(true)}>
                View
            </Button>
            <EricssonModal
                show={modalShow}
                onHide={() => setModalShow(false)}
            />
        </div>
    );
};

export default Ericsson;
