import React, { useRef, useLayoutEffect } from 'react';
import Typed from 'typed.js';
import {AiFillGithub, AiFillGitlab, AiFillLinkedin} from "react-icons/all";

const Hero = () => {
    // Create reference to store the DOM element containing the animation
    const el = useRef<HTMLSpanElement>(null);
    // Create reference to store the Typed instance itself
    const typed = useRef<Typed>(null);

    useLayoutEffect(() => {
        const options = {
            strings: [
                "a Designer", "a Developer", "a Software Architect", "a Swimmer"
            ],
            typeSpeed: 50,
            backSpeed: 50,
            loop: true
        };

        // elRef refers to the <span> rendered below
        // @ts-ignore
        typed.current = new Typed(el.current, options);

        return () => {
            // Make sure to destroy Typed instance during cleanup
            // to prevent memory leaks
            if (null != typed.current) {
                typed.current.destroy();
            }

        }
    }, [])
    return (
        <section id="hero" className="d-flex justify-content-between align-items-center">
            <div className="hero-container flex-grow-1 d-flex flex-column justify-content-center align-items-center" data-aos="fade-in">
                <h1>Miguel Angel Julián</h1>
                <div className="type-wrap">
                    <p>I'm <span className="typed" ref={el} /></p>
                </div>
            </div>
            <div className="social-links mt-3 text-center d-flex flex-column">
                <a href="https://github.com/thar" className="github"><AiFillGithub/></a>
                <a href="https://gitlab.com/djthar" className="gitlab"><AiFillGitlab/></a>
                <a href="https://www.linkedin.com/in/miguelangeljulianaguilar/" className="linkedin"><AiFillLinkedin/></a>
            </div>
        </section>
    );
};

export default Hero;
