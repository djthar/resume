import React from "react";
import Skills from "./Skills";

const Languages = () => {
    const languages : [string, number][] = [
        ["C++", 95],
        ["C", 90],
        ["Python", 90],
        ["UML", 90],
        ["HTML", 75],
        ["Javascript", 75],
        ["Go", 70],
        ["Scala", 70],
        ["Typescript", 65],
        ["Java", 65],
    ]
    const content = "In my professional career I've been exposed to many programming languages.<br/>From time to time I learn new ones to keep in pace with the advances in the field. While learning them I try to apply my previous knowledge but maintaining the community best practices of the new language.";

    return (
        <Skills skills={languages} title={"Programming Languages"} sectionBg={"section-bg"} sectionId={"languages"} content={content} />
    );
};

export default Languages;
