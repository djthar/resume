import React from "react";
import Profile from "./Profile";
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { BiHome, BiUser, BiFileBlank, BiServer, BiEnvelope } from "react-icons/bi";
import {IoLanguageSharp} from "react-icons/all";

const NavBar = () => {
    return (
        <Navbar expand="lg" sticky={"top"} bg="dark" variant="dark" style={{fontSize: "large", padding: 0}}>
            <Navbar.Brand href="#"><Profile/></Navbar.Brand>
            <Navbar.Toggle aria-controls="navbarScroll" />
            <Navbar.Collapse id="navbarScroll">
                <Nav className="mr-auto my-2 my-lg-0" navbarScroll>
                    <Nav.Link href="#hero"><BiHome /><span style={{padding: 10}}>Home</span></Nav.Link>
                    <Nav.Link href="#about"><BiUser/><span style={{padding: 10}}>About</span></Nav.Link>
                    <Nav.Link href="#languages"><IoLanguageSharp/><span style={{padding: 10}}>Programming Languages</span></Nav.Link>
                    <Nav.Link href="#technologies"><BiServer/><span style={{padding: 10}}>Technologies</span></Nav.Link>
                    <Nav.Link href="#resume"><BiFileBlank/><span style={{padding: 10}}>Resume</span></Nav.Link>
                    <Nav.Link href="#contact"><BiEnvelope/><span style={{padding: 10}}>Contact</span></Nav.Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
};

export default NavBar;
