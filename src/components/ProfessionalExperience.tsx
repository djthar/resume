import Ericsson from "./Ericsson";
import Competispy from "./Competispy";
import TheCorpora from "./TheCorpora";
import React from "react";

const ProfessionalExperience = () => {
    return (
        <div className="col-lg-6" data-aos="fade-up" data-aos-delay="100">
            <h3 className="resume-title">Professional Experience</h3>
            <Ericsson/>
            <Competispy/>
            <TheCorpora/>
        </div>
    );
};

export default ProfessionalExperience;
