import React from "react";

const Profile = () => {
    return (
        <div className="profile navbar-brand" style={{padding: 10}}>
            <h4 className="text-light">Miguel Angel</h4>
        </div>
    );
};

export default Profile;
