import React from "react";
import Education from "./Education";
import ProfessionalExperience from "./ProfessionalExperience";

const Resume = () => {
    return (
        <section id="resume" className="resume section-bg">
            <div className="container">
                <div className="section-title">
                    <h2>Resume</h2>
                </div>
                <div className="row">
                    <ProfessionalExperience/>
                    <Education/>
                </div>
            </div>
        </section>
    );
};

export default Resume;
