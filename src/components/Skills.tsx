import React, {useLayoutEffect, useRef} from "react";
import {ProgressBar} from "react-bootstrap";

const Skills = (props: {skills: [string, number][], sectionId: string, sectionBg: string, title: string, content: string}) => {
    const elementsPerCol = props.skills.length/2;
    const sectionClasses = "skills " + props.sectionBg;
    const listItemsLeft = props.skills.slice(0, elementsPerCol).map((skill) =>  <div className="progress">
        <span className="skill">{skill[0]} <i className="val">{skill[1]}%</i></span>
        <ProgressBar now={skill[1]}/>
    </div>);
    const listItemsRight = props.skills.slice(elementsPerCol).map((skill) =>  <div className="progress">
        <span className="skill">{skill[0]} <i className="val">{skill[1]}%</i></span>
        <ProgressBar now={skill[1]}/>
    </div>);
    const content = useRef<HTMLParagraphElement>(null);
    useLayoutEffect(() => {
        if (null != content.current) {
            content.current.innerHTML = props.content;
        }
    })
    return (
        <section id={props.sectionId} className={sectionClasses}>
            <div className="container">

                <div className="section-title">
                    <h2>{props.title}</h2>
                    <p ref={content}/>
                </div>

                <div className="row skills-content">
                    <div className="col-lg-6" data-aos="fade-up">
                        {listItemsLeft}
                    </div>
                    <div className="col-lg-6" data-aos="fade-up" data-aos-delay="100">
                        {listItemsRight}
                    </div>
                </div>

            </div>
        </section>
    );
};

export default Skills;
