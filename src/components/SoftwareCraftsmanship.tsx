import {BsPrefixProps, Omit} from "react-bootstrap/helpers";
import React, {useState} from "react";
import {Button, Modal, ModalProps} from "react-bootstrap";

const SoftwareCraftsmanshipModal = (props: JSX.IntrinsicAttributes & Omit<Pick<React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>, "key" | keyof React.HTMLAttributes<HTMLDivElement>> & { ref?: ((instance: HTMLDivElement | null) => void) | React.RefObject<HTMLDivElement> | null | undefined; }, BsPrefixProps<"div"> & ModalProps> & BsPrefixProps<"div"> & ModalProps & { children?: React.ReactNode; }) => {
    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Ericsson experience
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p>The topics studied in the master are focussed on giving the students the tools needed to design and build high quality software products.
                    <br/>
                    Covered topics:
                    <div className="row skills-content">
                        <div className="col-lg-6">
                            <ul>
                                <li>Clean Code, smell codes, SOLID principles and design patterns</li>
                                <li>Testing, Xtreme programming, TDD, BDD and refactoring</li>
                                <li>Functional programming</li>
                            </ul>
                        </div>
                        <div className="col-lg-6">
                            <ul>
                                <li>Parallel programming</li>
                                <li>Requisites, UML and Rational Unified process</li>
                                <li>Software architectures</li>
                            </ul>
                        </div>
                    </div>
                </p>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={props.onHide}>Close</Button>
            </Modal.Footer>
        </Modal>
    );
};

const SoftwareCraftsmanship = () => {
    const [modalShow, setModalShow] = useState(false);
    return (
        <div className="resume-item">
            <h4>Master of Software Craftsmanship</h4>
            <h5>2017 - 2019</h5>
            <p><em>Universidad Politectnica de Madrid, Madrid, Madrid</em></p>
            <Button variant="primary" onClick={() => setModalShow(true)}>
                View
            </Button>
            <SoftwareCraftsmanshipModal
                show={modalShow}
                onHide={() => setModalShow(false)}
            />
        </div>
    );
};

export default SoftwareCraftsmanship;
