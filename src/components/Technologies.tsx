import React from "react";
import Skills from "./Skills";

const Technologies = () => {
    const technologies : [string, number][] = [
        ["CMake", 95],
        ["GoogleTest", 95],
        ["Jenkins", 90],
        ["Gerrit", 90],
        ["Kubernetes", 85],
        ["Docker", 85],
        ["React", 80],
        ["Flask", 80],
        ["Prometheus", 75],
        ["MongoDB", 75],
        ["Gitlab-CI", 70],
        ["ROS", 70],
        ["WebSocket", 70],
        ["Terraform", 65],
        ["ArgoCD", 65],
        ["Github Actions", 65],
    ]
    const content = "Similar than with the programming languages I've been exposed to many different technologies.\n" +
        "                    I also learn new ones by applying them in my personal projects and if some one is suitable for its\n" +
        "                    use in my professional job I apply it there.<br/>\n" +
        "                    I like this approach because the real knowledge comes with the practice.</p>"

    return (
        <Skills skills={technologies} title={"Technologies"} sectionBg={""} sectionId={"technologies"} content={content} />
    );
};

export default Technologies;
