import {BsPrefixProps, Omit} from "react-bootstrap/helpers";
import React, {useState} from "react";
import {Button, Modal, ModalProps} from "react-bootstrap";

const TheCorporaModal = (props: JSX.IntrinsicAttributes & Omit<Pick<React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>, "key" | keyof React.HTMLAttributes<HTMLDivElement>> & { ref?: ((instance: HTMLDivElement | null) => void) | React.RefObject<HTMLDivElement> | null | undefined; }, BsPrefixProps<"div"> & ModalProps> & BsPrefixProps<"div"> & ModalProps & { children?: React.ReactNode; }) => {
    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    TheCorpora experience
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <ul>
                    <li>Developed several systems in C++ and Python for a small robot using ROS framework such as telepresence, object recognition, gesture recognition or gesture controller music player</li>
                    <li>Designed the manufacturing test chain with its semi-automatic acceptance tests</li>
                    <li>Drove several presentations to clients</li>
                    <li>Carried the FCC and CE tests for the robot
                    </li>
                </ul>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={props.onHide}>Close</Button>
            </Modal.Footer>
        </Modal>
    );
};

const TheCorpora = () => {
    const [modalShow, setModalShow] = useState(false);
    return (
        <div className="resume-item">
            <h4>Software and Hardware Engineer</h4>
            <h5>2010 - 2013</h5>
            <p><em>The Corpora, Madrid, Madrid</em></p>
            <Button variant="primary" onClick={() => setModalShow(true)}>
                View
            </Button>
            <TheCorporaModal
                show={modalShow}
                onHide={() => setModalShow(false)}
            />
        </div>
    );
};

export default TheCorpora;
